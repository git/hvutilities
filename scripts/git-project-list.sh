#!/bin/bash

SCRIPT="`readlink -e $0`"
SCRIPTPATH=$(dirname ${SCRIPT})
source ${SCRIPTPATH}/hv-git-functions.sh

print_usage()
{
    echo "$(basename $0) -- List last SHA for each repo"
    echo "Usage: $(basename $0) [OPTIONS...]"
}

if [ "x${1}" = "x--help" ]; then
    print_usage
    exit 1
fi

hv_git_validate_subrepos_list

list_revisions()
{
    if [ ${#} -ne 1 ]; then
        echo "Missing repository name"
        exit 1
    fi

    local r=${1}

    pushd "${r}" 1> /dev/null
    local sha=$(git log --abbrev=10 --pretty='format:%h %s' HEAD~1..HEAD)

    local line='             '
    printf "%s %s  %s\n" ${r} "${line:${#r}}" "${sha}"
    popd 1> /dev/null
}

for r in ./ $(hv_git_get_subrepos_list); do
    # Make sure directory exists
    if [ ! -d "${r}" ]; then
        echo "Missing repos ${r}, skipping"
        continue
    fi

    list_revisions ${r}
done
