#!/bin/bash

# Uncomment to test
#PATCH_OPTS="--dry-run"

GPLPATCH=/tmp/gpl.patch

# The patch was created with:
#   $>  diff -Nar -u1 a/file b/file > gpl.patch
create_patch1()
{
    cat > ${GPLPATCH} << EOF
--- a/file	2014-03-22 00:46:49.000000000 -0400
+++ b/file	2014-03-23 14:19:51.000000000 -0400
@@ -5,15 +5,3 @@
  *
- * This program is free software; you can redistribute it and/or modify
- * it under the terms of the GNU General Public License as published by
- * the Free Software Foundation; either version 2 of the License, or
- * (at your option) any later version.
- *
- * This program is distributed in the hope that it will be useful,
- * but WITHOUT ANY WARRANTY; without even the implied warranty of
- * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
- * GNU General Public License for more details.
- *
- * You should have received a copy of the GNU General Public License
- * along with this program; if not, write to the Free Software
- * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
+ * This file is released under the GPLv2
  */
EOF
}

create_patch2()
{
    cat > ${GPLPATCH} << EOF
--- a/file	2014-03-22 00:46:49.000000000 -0400
+++ b/file	2014-03-23 14:19:51.000000000 -0400
@@ -5,15 +5,3 @@
  *
- * This program is free software; you can redistribute it and/or modify
- * it under the terms of the GNU Lesser General Public License as published by
- * the Free Software Foundation; either version 2.1 of the License, or
- * (at your option) any later version.
- *
- * This program is distributed in the hope that it will be useful,
- * but WITHOUT ANY WARRANTY; without even the implied warranty of
- * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
- * GNU Lesser General Public License for more details.
- *
- * You should have received a copy of the GNU Lesser General Public License
- * along with this program; if not, write to the Free Software
- * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
+ * This file is released under the GPLv2
  */
EOF
}

create_patch3()
{
    cat > ${GPLPATCH} << EOF
--- a/file	2014-03-22 00:46:49.000000000 -0400
+++ b/file	2014-03-23 14:19:51.000000000 -0400
@@ -5,15 +5,3 @@
  *
- * This program is free software; you can redistribute it and/or modify
- * it under the terms of the GNU General Public License as published by
- * the Free Software Foundation; either version 2 of the License, or
- * (at your option) any later version.
- *
- * This program is distributed in the hope that it will be useful,
- * but WITHOUT ANY WARRANTY; without even the implied warranty of
- * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
- * GNU General Public License for more details.
- *
- * You should have received a copy of the GNU General Public License
- * along with this program; if not, write to the Free Software
- * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
+ * This file is released under the GPLv2
  */
EOF
}

for f in *.c *.h; do
    if cat ${f} | grep -q " MA 02139" ; then
        create_patch1
    elif cat ${f} | grep -q " MA 02111-1307" ; then
        create_patch2
    elif cat ${f} | grep -q " MA 02110-1301" ; then
        create_patch3
    else
        echo "GPL header not found in ${f}, skipping"
        continue
    fi

    sed -i -e "s@/file@/${f}@g" ${GPLPATCH}

    patch ${PATCH_OPTS} -Np1 -i ${GPLPATCH}

    rm ${GPLPATCH}
done
