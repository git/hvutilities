#!/bin/bash

SCRIPT="`readlink -e $0`"
SCRIPTPATH=$(dirname ${SCRIPT})
source ${SCRIPTPATH}/hv-git-functions.sh

print_usage()
{
    echo "$(basename $0) -- Start gitk for each subrepository"
    echo "Usage: $(basename $0) [OPTIONS...]"
}

if [ "x${1}" = "x--help" ]; then
    print_usage
    exit 1
fi

if [ ${#} -ne 0 ]; then
    echo "Unsupported argument(s)"
    print_usage
    exit 1
fi

hv_git_validate_subrepos_list

branch=${1}
rc=0

for r in ./ $(hv_git_get_subrepos_list); do
    # Make sure directory exists
    if [ ! -d "${r}" ]; then
        echo "Skipping missing repos ${r}"
        continue
    fi

    pushd ${r} 1> /dev/null
    # Use absolute path to prevent using an alias
    /usr/bin/gitk --all &
    popd 1> /dev/null
    rc=${?}
done

exit ${rc}
