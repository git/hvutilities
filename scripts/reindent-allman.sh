#!/bin/sh

ASTYLE=`which astyle`
OPTIONS="--style=allman \
    --mode=c \
    --indent=spaces=4 \
    --pad-oper \
    --align-pointer=name \
    --pad-header \
    --min-conditional-indent=0 \
    --indent-col1-comments \
    --convert-tabs \
    --recursive \
    --suffix=none"

${ASTYLE} ${OPTIONS} "*.c"
${ASTYLE} ${OPTIONS} "*.cpp"
${ASTYLE} ${OPTIONS} "*.h"
${ASTYLE} ${OPTIONS} "*.hpp"
