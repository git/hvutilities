#!/bin/sh

print_usage()
{
    echo "pstopdf -- Translates multiple Postscript files into a"
    echo "           unique PDF document."
    echo "Usage: $(basename $0) [OPTION]... [FILE]..."
    echo
    echo "  -o filename     Specify the name of the generated PDF file. The"
    echo "                  default is 'output.pdf'."
}

while getopts ":o:" opt; do
    case $opt in
	o )
	    PDF_FILE=$OPTARG
	    ;;
	\? )
	    print_usage
	    ;;
    esac
done

if [ -z "${PDF_FILE}" ]; then
    PDF_FILE="output.pdf"
fi

shift $(($OPTIND - 1))

if [ $# -eq 0 ]; then
    print_usage
    exit 1
fi

gs -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=${PDF_FILE} $@

exit 0
