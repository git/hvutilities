#!/bin/bash

SCRIPT="`readlink -e $0`"
SCRIPTPATH=$(dirname ${SCRIPT})
source ${SCRIPTPATH}/hvk-common.sh

KVER_BASE=$(make kernelversion)

# -W:
# Not all compiler warnings are enabled by default. Build the
# kernel with "make EXTRA_CFLAGS=-W" to get the full set.
#
# C=1: run Sparse semantic checker for C programs
# W=1: Enable extra build checks:
#   1: warnings which may be relevant and do not occur too often
# V=1: Disable Makefile silent mode


# Temporaire: désactivé W=1 car cause un problème avec kernel 5.17-rc...
#KMAKE="${KMAKE} W=1 C=1 KCFLAGS=-W"
KMAKE="${KMAKE} C=1 KCFLAGS=-W"

# The path to the specific drivers we want to compile:
DRIVER_PATH[0]=drivers/rtc
DRIVER_NAME[0]=rtc-pcf2127

DRIVER_PATH[1]=drivers/iio/adc
DRIVER_NAME[1]=ti-ads7924

if [ ${#} -eq 1 ]; then
    EVK_IP="${1}"
fi

k=0
for driver_path in "${DRIVER_PATH[@]}"; do
    p="${DRIVER_PATH[${k}]}"
    n="${DRIVER_NAME[${k}]}"

    ${KMAKE} M=${p}

    if [ x"${EVK_IP}" != x"" ]; then
	scp ${p}/${n}.ko root@${EVK_IP}:/lib/modules/${KVER_BASE}*/kernel/${p}/
    fi

    k=$((${k} + 1))
done

${KMAKE} dtbs

${KMAKE} samples

if [ x"${EVK_IP}" != x"" ]; then
    scp ${DTB_SRC} root@${EVK_IP}:${BOOT_DEST}/${DTB_DEST}
    if [ -f samples/rtc-test/rtc-test ]; then
	scp samples/rtc-test/rtc-test root@${EVK_IP}:
    fi

    #ssh root@${EVK_IP} "/sbin/depmod -a"
fi
