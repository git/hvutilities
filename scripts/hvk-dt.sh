#!/bin/bash

SCRIPT="`readlink -e $0`"
SCRIPTPATH=$(dirname ${SCRIPT})
source ${SCRIPTPATH}/hvk-common.sh

if [ x"${DTS_BOARD}" != x"" ]; then
    DTB_FILES="${DTB_FILES} ${DTS_BOARD}"
fi

if [ "${DTS_SUBDIR}" != "" ]; then
    dts_subdir="${DTS_SUBDIR}/"
fi

# Add .dtb extension to all DTB files:
dtb_files=""
for f in ${DTB_FILES}; do
    dtb_files="${dtb_files} ${dts_subdir}${f}.dtb"
done

if [ x"${dtb_files}" != x"" ]; then
    # See https://lore.kernel.org/lkml/20221102214654.axyptitp5kpq3wcq@notapiano/T/
    ${KMAKE} CHECK_DTBS=y ${dtb_files}
fi

DT_CHECKER="${KMAKE} DT_CHECKER_FLAGS=-m dt_binding_check"

# Remove leading whitespace to avoid the Makefile interpreting DT_SCHEMA_FILES
# as empty like in this example:
#     ${DT_CHECKER} DT_SCHEMA_FILES=     Documentation/devicetree/bindings...
DT_SCHEMA_FILES=$(echo ${DT_SCHEMA_FILES} | xargs)

if [ x"${DT_SCHEMA_FILES}" != x"" ]; then
    ${DT_CHECKER} DT_SCHEMA_FILES=${DT_SCHEMA_FILES}
fi

# To check all:
#${DT_CHECKER}

#${KMAKE} checkstack
