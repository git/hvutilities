#!/bin/bash

SCRIPT="`readlink -e $0`"
SCRIPTPATH=$(dirname ${SCRIPT})
source ${SCRIPTPATH}/hvk-common.sh

case ${DEFCONFIG_BOARD} in
    all*)
        "Error, cannot save defconfig for \"${DEFCONFIG_BOARD}\""
        exit 1
esac

cfg_prefix="arch/${ARCH}/"

if [ x"${mode}" = x"uboot" ]; then
    cfg_prefix=""
fi

${KMAKE} savedefconfig
cp defconfig ${cfg_prefix}configs/${DEFCONFIG_BOARD}_defconfig
