#!/bin/bash

MOUNT_POINTS="\
    / \
    /mnt/rescue \
    /mnt/hvlinux-latest \
    /mnt/mint \
    /mnt/debian \
    /mnt/stockage"

for mount in ${MOUNT_POINTS}; do
    fstrim ${mount}
done
