#!/bin/sh

ASTYLE=`which astyle`
OPTIONS="--style=linux \
    --lineend=linux \
    --indent=force-tab=8 \
    --min-conditional-indent=0 \
    --align-pointer=name \
    --pad-oper \
    --pad-header \
    --indent-col1-comments \
    --convert-tabs \
    --recursive \
    --suffix=none"

${ASTYLE} ${OPTIONS} "*.c"
${ASTYLE} ${OPTIONS} "*.cpp"
${ASTYLE} ${OPTIONS} "*.h"
${ASTYLE} ${OPTIONS} "*.hpp"
