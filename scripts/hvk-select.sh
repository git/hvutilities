#!/bin/bash

SCRIPT="`readlink -e $0`"
SCRIPTPATH=$(dirname ${SCRIPT})
source ${SCRIPTPATH}/hvk-common.sh

hvk_src="${HOME}/.hvk"
hvk_link=".hvk"
dialog_outfile="$(mktemp -t hvk.XXXXXXXXXX)"

if [ ! -d "${hvk_src}" ]; then
    echo "Missing HVK source directory: ${hvk_src}"
    exit 1
fi

file=()
count=0
list=""
i=1

# Loop over source folder and add files to array
while IFS= read -r -d $'\0' file; do
    f="$(basename ${file})"
    file+=("${f}")

    status="off"

    set +e
    hvk_current="$(readlink -e .hvk)"
    set -e

    if [ "${hvk_current}" != "" ]; then
        hvk_current_base="$(basename ${hvk_current})"

        if [ "${f}" = "${hvk_current_base}" ]; then
            status="on"
        fi
    fi

    list="${list} ${i} ${f} ${status}"
    count=$((${count} + 1))
    i=$((${i} + 1))
done < <(find "${hvk_src}" -maxdepth 1 -type f ! -name '*~' -print0)

if [ ${count} -eq 0 ]; then
    echo "Error: no board files found"
    exit 1
fi

rows=$((${count} + 7))
width=40

dialog --radiolist "HVK board selection" ${rows} ${width} ${count} ${list} 2> ${dialog_outfile}

id=$(cat ${dialog_outfile})
sel="${file[${id}]}"

rm -f ${hvk_link}
ln -s "${hvk_src}/${sel}" ${hvk_link}

rm ${dialog_outfile}

clear
