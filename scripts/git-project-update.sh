#!/bin/bash

SCRIPT="`readlink -e $0`"
SCRIPTPATH=$(dirname ${SCRIPT})
source ${SCRIPTPATH}/hv-git-functions.sh

print_usage()
{
    echo "$(basename $0) -- GIT update to branch for multiple subrepositories"
    echo "Usage: $(basename $0) [OPTIONS...] BRANCH"
}

if [ "x${1}" = "x--help" ]; then
    print_usage
    exit 1
fi

if [ ${#} -ne 1 ]; then
    echo "Missing BRANCH argument"
    print_usage
    exit 1
fi

hv_git_validate_subrepos_list

branch=${1}
rc=0

for r in ./ $(hv_git_get_subrepos_list); do
    # Make sure directory exists
    if [ ! -d "${r}" ]; then
        echo "Skipping missing repos ${r}"
        continue
    fi

    hv_git_checkout ${r} ${branch}
    rc=${?}
done

exit ${rc}
