#!/bin/bash

PROG_NAME=$(basename $0)
SCRIPT="`readlink -e $0`"
SCRIPTPATH=$(dirname ${SCRIPT})
source ${SCRIPTPATH}/hvk-common.sh

DEPLOYDIR=rootfs

print_usage()
{
    echo "${PROG_NAME} -- U-Boot/kernel compile script"
    echo "Usage: ${PROG_NAME} [OPTIONS...] [IP ADDRESS]"
    echo
    echo "Options:"
    echo "  -e   change extlinux default menu to test"
    echo "  -f   deploy folder (default is ./rootfs)"
    echo "  -h   display this help and exit"
    echo "  -r   reboot after deploying files"
    echo "  -w   compile with W=1"
    echo "  -z   create archive"
    echo
}

reboot=0
warnings_opts=""
archive=0
deploydir=${DEPLOYDIR}
extlinux=0

while getopts "ef:hrwz" flag ;do
    case ${flag} in
	h)
	    print_usage
            exit 0
	    ;;
	e)
            extlinux=1
	    ;;
	f)
            deploydir=${OPTARG}
	    ;;
	r)
            reboot=1
	    ;;
        w)
            warnings_opts="W=1"
	    ;;
        z)
            archive=1
	    ;;
	?)
	    echo "${PROG_NAME}: Invalid option: ${OPTARG}."
	    echo "Try \`${PROG_NAME} -h' for more information."
	    exit 1
	    ;;
    esac
done
shift `expr "${OPTIND}" - 1`

# `$#' now represents the number of arguments after the options.
# `$1' is the first argument, etc.
if [ $# -gt 1 ]; then
    echo "${PROG_NAME}: Too many arguments."
    echo "Try \`${PROG_NAME} -h' for more information."
    exit 1
fi

if [ ${#} -eq 1 ]; then
    EVK_IP="${1}"
fi

if [ "${reboot}" = "1" -a "${archive}" = "1" ]; then
    echo "${PROG_NAME}: You must specify only one of \"-r\" or \"-z\" option"
    echo "Try \`${PROG_NAME} -h' for more information."
    exit 1
fi

if [ ! -f .config ]; then
    echo "Missing configuration file .config."
    echo "Configure your kernel by running 'hvk-init.sh'"
    exit 1
fi

KMAKE="${KMAKE} ${warnings_opts}"

if grep -q '^CONFIG_MODULES=y' .config; then
    CONFIG_MODULES=1
else
    CONFIG_MODULES=0
fi

${KMAKE}

if [ x"${mode}" = x"linux" ]; then
    if [ x"${CONFIG_MODULES}" = x"1" ]; then
        ${KMAKE} modules_prepare
        ${KMAKE} modules
    fi

    if [ x"${DTB_SRC}" != x"" ]; then
        ${KMAKE} dtbs
    fi
elif [ x"${mode}" = x"uboot" ]; then
    ${KMAKE} u-boot-initial-env
fi

function rootfs_install() {
    mkdir -p ${deploydir}

    # Clean old modules directory:
    rm -rf ${deploydir}/lib/modules

    if [ x"${CONFIG_MODULES}" = x"1" ]; then
        ${KMAKE} INSTALL_MOD_PATH=${deploydir} modules_install
    fi

    mkdir -p ${deploydir}/${BOOT_DEST}
    rm -rf ${deploydir}/${BOOT_DEST}/*

    cp ${BOOT_SRC}/${IMG_SRC} ${deploydir}/${BOOT_DEST}/${IMG_DEST}

    DTB_MAIN=""
    for f in ${DTB_FILES}; do
        if [ "${DTB_MAIN}" = "" ]; then
            DTB_MAIN="${f}.dtb"
        fi

        # Copy dtb and dtbo files:
        cp ${DTS_SRC}/${f}.dtb* ${deploydir}/${BOOT_DEST}/
    done

    # Create symbolic link to main DTB:
    ln -s ${DTB_MAIN} ${deploydir}/${BOOT_DEST}/test.dtb
}

function rootfs_archive() {
    if [ -f "include/config/kernel.release" ]; then
        archive="linux-$(cat include/config/kernel.release).tar.xz"
    fi

    if [ "${archive}" = "" ]; then
        echo "${PROG_NAME}: Unable to determine kernel release"
        exit 1
    fi

    pushd ${deploydir}
    tar cf - * | xz > ../${archive}
    popd
}

if [ "${archive}" = "1" ]; then
    rootfs_install
    rootfs_archive
fi

if [ "${EVK_IP}" != "" ]; then
    rootfs_install
    rootfs_archive

    scp ${archive} root@${EVK_IP}:/tmp
    ssh root@${EVK_IP} "cd / && tar -xf /tmp/${archive}"

    if [ ${extlinux} -eq 1 ]; then
        # Determine if using extlinux on target:
        ssh -q root@${EVK_IP} [[ -f /boot/extlinux/extlinux.conf ]] && EXTLINUX=1 || EXTLINUX=0;

        if [ "${EXTLINUX}" = "1" ]; then
            # Switch to test kernel and DTB:
            ssh root@${EVK_IP} "sed -i -e 's@^DEFAULT.*@DEFAULT test@' /boot/extlinux/extlinux.conf"

            # Determine if using boot_prefix variable U-Boot on target:
            ssh -q root@${EVK_IP} "fw_printenv boot_prefix | grep -q boot_prefix" && BOOT_PREFIX=1 || BOOT_PREFIX=0;

            if [ "${BOOT_PREFIX}" = "1" ]; then
                # Optionally set boot_prefix variable:
                ssh root@${EVK_IP} "fw_setenv boot_prefix boot/test"
            fi
        fi
    fi

    if [ x"${reboot}" = x"1" ]; then
        ssh root@${EVK_IP} "reboot"
    fi
fi
