#!/bin/bash

USERDIR=/home/myuser

BACKUP_DIR=${USERDIR}/backup
MYSQL_BACKUP_DIR=${BACKUP_DIR}/mysql

MYSQL_DATABASES="\
    mysql \
    db1"

read -s -p "Enter MySQL password: " password

if [ "x${password}" == "x" ]; then
    echo "Error, empty MySQL password"
    exit 1
fi

if [ ! -d ${BACKUP_DIR} ]; then
    mkdir ${BACKUP_DIR}
fi

if [ ! -d ${MYSQL_BACKUP_DIR} ]; then
    mkdir ${MYSQL_BACKUP_DIR}
fi

for db in ${MYSQL_DATABASES}; do
    mysqldump -u root -p${password} ${db} > ${MYSQL_BACKUP_DIR}/${db}.sql
done

# Compact GIT repositories
for r in /srv/git/*; do
    if [ -f ${r}/HEAD ]; then
	pushd ${r}
	git gc --aggressive
	popd
    fi
done

# Backup GIT repositories
tar cf - /srv/git | bzip2 -9 > ${BACKUP_DIR}/git.tar.bz2

# Backup web sites
tar cf - /srv/www | bzip2 -9 > ${BACKUP_DIR}/www.tar.bz2

# Backup emails
tar cf - ${USERDIR}/.mail | bzip2 -9 > ${BACKUP_DIR}/mail.tar.bz2
