#!/bin/bash

# Global variable: dest
display_subrepo_name()
{
    COLOR_BLUE='\033[1;34m'
    COLOR_NORMAL='\033[0m'

    echo -en "${COLOR_BLUE}"
    echo "${dest}"
    echo -en "${COLOR_NORMAL}"
}

PROJRC_COUNT=0
SUBPATHS_COUNT=0

# We parse the projrc file in the assembly (on the server), to check
# for any projrc substitutions.
#
# Arg 1: repository (path and name)
#
# Return the substitution key/values in arrays:
#  PROJRC_KEY
#  PROJRC_VAL
# PROJRC_COUNT will be greater than zero if any substitutions were found.
hg_parse_projrc() {
    if [ ${#} -lt 1 ]; then
        echo "Missing repository name"
    fi

    # Repo name can have spaces in it, hence the use of ${*}
    local repo="${*}"

    if [ -f "${repo}/.hg/projrc" ]; then

        TMPF="/tmp/$(basename $0).projrc-$$.tmp"

        # Create temporary copy to replace backslashes with slashes.
        # Make sure to only replace path backslashes, not backslashes for
        # quoting spaces.
        # \\[^ ]  Match a backslash, not followed by a space
        cat "${repo}/.hg/projrc" | sed 's/\\\([^ ]\)/\/\1/g' > ${TMPF}

        while read p; do
            if echo ${p} | grep -q "\s=\s"; then
                local key=$(echo ${p} | sed 's/^\(.*\) = .*/\1/')
                local val=$(echo ${p} | sed 's/.* = \(.*\)/\1/')

                PROJRC_KEY[${PROJRC_COUNT}]="${key}"
                PROJRC_VAL[${PROJRC_COUNT}]="${val}"

                if [ -n "${debug}" ]; then
                    echo "Parsing PROJRC entry: ${p}"
                    echo "  key: ${key}"
                    echo "  val: ${val}"
                fi

                PROJRC_COUNT=$((PROJRC_COUNT + 1))
            fi
        done < ${TMPF}

        rm ${TMPF}
    fi
}

# git-remote-hg doesn't work with Mercurial subpaths extension,
# so use it manually.
#
# TODO: Replace this function with hg_parse_subpaths() and
#       apply_substitutions().
hg_subpaths_config()
{
    subpaths=$(hg showconfig | grep "subpaths")

    if [ -n "${subpaths}" ]; then
        sp_src=${subpaths//subpaths./}
        sp_src=${sp_src//=*/}
        sp_src=${sp_src//\\/} # Remove windows separator (LSI)
        sp_dst=${subpaths//*=/}

        if [ -n "${debug}" ]; then
            echo "sp_src = $sp_src"
            echo "sp_dst = $sp_dst"
        fi
    fi
}

# We parse the Mercurial configuration, to check
# for any SUBPATHS substitutions.
#
# Return the substitution key/values in arrays:
#  SUBPATHS_KEY
#  SUBPATHS_VAL
# SUBPATHS_COUNT will be greater than zero if any substitutions were found.
hg_parse_subpaths()
{
    subpaths=$(hg showconfig subpaths)

    if [ -n "${subpaths}" ]; then
        for p in ${subpaths}; do
            local key=$(echo ${p} | sed 's/^subpaths\.\(.*\)=.*/\1/')
            local val=$(echo ${p} | sed 's/.*=\(.*\)/\1/')

            # Replace '\\' with '/'
            key=${key//\\\\/\/}
            #        ^^ replace multiple times

            val=${val//\\\\/\/}

            SUBPATHS_KEY[${SUBPATHS_COUNT}]="${key}"
            SUBPATHS_VAL[${SUBPATHS_COUNT}]="${val}"

            if [ -n "${debug}" ]; then
                echo "Parsing SUBPATHS entry: ${p}"
                echo "  key: ${key}"
                echo "  val: ${val}"
            fi

            SUBPATHS_COUNT=$((SUBPATHS_COUNT + 1))
        done
    fi
}

# Replace the given path with either entries from PROJRC or SUBPATHS
# Arg 1: path
apply_substitutions()
{
    # Repo name can have spaces in it, hence the use of ${*}
    local path="${*}"

    if [ ${PROJRC_COUNT} -ne 0 ]; then
        for s in $(seq 0 $((${PROJRC_COUNT} - 1))); do
            local pattern=${PROJRC_KEY[${s}]}
            local replacement=${PROJRC_VAL[${s}]}

            path=${path/${pattern}/${replacement}}
        done
    fi

    if [ ${SUBPATHS_COUNT} -ne 0 ]; then
        for s in $(seq 0 $((${SUBPATHS_COUNT} - 1))); do
            local pattern=${SUBPATHS_KEY[${s}]}
            local replacement=${SUBPATHS_VAL[${s}]}

            path=${path/${pattern}/${replacement}}
        done
    fi

    echo "${path}"
}

# Map a revision to a branch name in HG subrepository
# Use hg log (in original repo) to get branch name corresponding to that
# revision.
#
# Arg 1: subrepository URL
# Arg 2: id
# Arg 3: revision
subrepo_find_branch()
{
    local src="${1}"
    local id="${2}"
    local rev="${3}"

    pushd "${src}" 1> /dev/null

    branch=$(hg log -r ${rev} | grep "branch:" | sed "s/branch:\ *//")

    if [ -z "${branch}" ]; then
        # If "branch:" is null, this indicate we are on the default branch
        branch=default
    fi

    num=$(hg log --branch "${branch}" --template '{node}\n' | \
        grep -n ${rev} | awk -F ':' '{print $1}')

    if [ -n "${debug}" ]; then
        echo "  branch: ${branch}"
        echo "  num:    ${num}"
    fi

    popd 1> /dev/null
}
