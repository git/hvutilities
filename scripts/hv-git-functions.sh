#!/bin/bash

# Uncomment to have verbose debug output
#hv_git_debug=1

SUBREPOS_LIST=.gitsubrepos

hv_git_validate_subrepos_list()
{
    if [ ! -f ${SUBREPOS_LIST} ]; then
        echo "${FUNCNAME}: missing subrepos file list: ${SUBREPOS_LIST}"
        print_usage
        exit 1
    fi

    # Read list of repositories from file named .gitsubrepos
    while IFS=$'\n' read r ; do
        [[ "${r}" =~ \#.* ]] && continue # Skip comment lines
        [ -z ${r} ] && continue # Skip enmpty lines

        # If repo directory is absent, it is ok.
        # If repo directory is present, make sure it is a GIT repo
        if [ -d "${r}" ]; then
            if [ ! -d "${r}/.git" ]; then
                echo "${FUNCNAME}: not a GIT repository: ${r}"
                echo "BOZO=<${r}/.git>"
                exit 1
            fi
        fi
    done < ${SUBREPOS_LIST}

    return 0
}

hv_git_get_subrepos_list()
{
    local list=""
    local r=""
    local IFS=""

    # Read list of repositories from file named .gitsubrepos
    while IFS=$'\n' read r ; do
        [[ "${r}" =~ \#.* ]] && continue # Skip comment lines
        [ -z ${r} ] && continue # Skip enmpty lines

        # Add to list only if directory is present, and if it is a valid
        # GIT repo
        if [ -d "${r}/.git" ]; then
            list="${list} ${r}"
        fi
    done < ${SUBREPOS_LIST}

    echo "${list}"
}

# Arg 1: repository
# Arg 2: branch to checkout (or tag)
hv_git_checkout()
{
    if [ ${#} -ne 2 ]; then
        echo "${FUNCNAME}: missing arguments"
        exit 1
    fi

    local rc=0
    local r=${1}
    local b=${2}

    echo -n "Repo ${r}: "

    pushd "${r}" 1> /dev/null

    exists=$(git rev-parse -q --verify ${b})
    if [ -n "${exists}" ]; then
        vco -q ${b} 1> /dev/null

        if [ ${?} -ne 0 ]; then
            rc=1
            echo "${b} (error)"
        else
            echo "${b}"
        fi
    else
        echo "${b} (not found)"
        rc=1
    fi

    popd 1> /dev/null

    return ${rc}
}

# Create a new tag in repo
# Arg 1: repository
# Arg 2: branch from which to create tag
# Arg 3: new tag name
hv_git_tag_from_branch()
{
    if [ ${#} -ne 3 ]; then
        echo "${FUNCNAME}: missing arguments"
        exit 1
    fi

    local rc=0
    local sha=""
    local r=${1}
    local b=${2}
    local tag=${3}

    echo -n "Repo ${r}: "

    pushd "${r}" 1> /dev/null

    sha=$(git show-ref refs/heads/${b} | awk '{print $1}')
    if [ -n "${sha}" ]; then
        git tag ${tag} ${sha} 1> /dev/null
        rc=${?}

        if [ ${rc} -ne 0 ]; then
            echo "${tag} (error)"
        else
            echo "${tag}"
            git push --tags
        fi
    else
        echo "${FUNCNAME}: branch \"${b}\" not found"
        rc=1
    fi

    popd 1> /dev/null

    return ${rc}
}

# Encore utile?
# Arg1: repository
hv_git_get_head_sha()
{
    if [ ${#} -ne 1 ]; then
        echo "Missing repository name"
        exit 1
    fi

    local r=${1}

    pushd "${r}" 1> /dev/null
    local sha=$(git log --pretty='format:%H' HEAD~1..HEAD)
    echo "${sha}"
    popd 1> /dev/null
}
