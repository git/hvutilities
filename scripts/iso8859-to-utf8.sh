#!/bin/bash

TMPFILE=/tmp/iso8859-to-utf8.tmp

if [ ${#} -eq 0 ]; then
    echo "Erreur: aucun fichier(s) à traiter."
    exit 1
fi

for f in ${*} ; do
    if [ ! -f "${f}" ]; then
        echo "Erreur: fichier introuvable: ${f}"
        exit 1
    fi
    if file -i "${f}" | grep -q "iso-8859-1"; then
        echo "[ISO-8859 -> UTF8] ${f}"
        iconv -f iso-8859-1 -t utf-8 "${f}" > ${TMPFILE}
        mv ${TMPFILE} "${f}"
    fi
done

rm -f ${TMPFILE}
