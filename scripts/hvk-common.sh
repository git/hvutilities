#!/bin/bash

# Abort on errors
set -e

if [ -f include/linux/linux_logo.h ]; then
    mode=linux
elif [ -f include/asm-generic/u-boot.h ]; then
    mode=uboot
else
    echo "Error: no kernel or U-Boot repo detected, aborting."
    exit 1
fi

if [ -f .hvk ]; then
    # Source project definition file (in project's git repo):
    source .hvk
else
    # Source x86 generic definition file
    source ${SCRIPTPATH}/hvk-x86.sh
fi

: ${MAKEJOBS:="$(nproc)"}

if [ x"${MAKEJOBS}" != x"" ]; then
    MAKEJOBS_ARG="-j ${MAKEJOBS}"
fi

KMAKE="make ${MAKEJOBS_ARG}"

# Needed to compile sample userspace programs (rtc-test):
export CROSS_COMPILE_KCFLAGS=${KCFLAGS}

DEFCONFIG_OPT=""

case ${DEFCONFIG_BOARD} in
    all*)
        DEFCONFIG_OPT="${DEFCONFIG_BOARD}"
        ;;
    *)
        DEFCONFIG_OPT="${DEFCONFIG_BOARD}_defconfig"
        ;;
esac

if [ "${BOOT_SRC}" = "" ]; then
    BOOT_SRC="arch/${ARCH}/boot"
fi

if [ "${BOOT_DEST}" = "" ]; then
    BOOT_DEST="boot/test"
fi

if [ "${DTS_SRC}" = "" ]; then
    # DTS_SUBDIR is empty by default. Can be set to manufacturer subdir,
    # for example.
    DTS_SRC="arch/${ARCH}/boot/dts/${DTS_SUBDIR}"
fi

if [ "${IMG_SRC}" = "" ]; then
    IMG_SRC="Image"
fi

if [ "${IMG_DEST}" = "" ]; then
    IMG_DEST="Image"
fi

# Arg1: src file
# Arg2: destination user@host
copy_exec()
{
    scp ${1} ${2}:/tmp
    ssh ${2} "install -m 755 /tmp/$(basename ${1}) \${HOME}"
}
