#!/bin/bash

SCRIPT="`readlink -e $0`"
SCRIPTPATH=$(dirname ${SCRIPT})
source ${SCRIPTPATH}/hv-git-functions.sh

print_usage()
{
    echo "$(basename $0) -- GIT remote update for multiple subrepositories"
    echo "Usage: $(basename $0) [OPTIONS...]"
}

if [ "x${1}" = "x--help" ]; then
    print_usage
    exit 1
fi

hv_git_validate_subrepos_list

remote_update_repo()
{
    if [ ${#} -ne 1 ]; then
        echo "Missing repository name"
        exit 1
    fi

    local r=${1}

    echo "Repo ${r}:"

    pushd "${r}" 1> /dev/null
    git fetch --all --prune 1> /dev/null
    vco master
    git pull --ff-only
    vco latest
    git pull --ff-only
    popd 1> /dev/null
}

for r in ./ $(hv_git_get_subrepos_list); do
    # Make sure directory exists
    if [ ! -d "${r}" ]; then
        echo "Missing repos ${r}, skipping"
        continue
    fi

    remote_update_repo ${r}
    rc=${?}
done
