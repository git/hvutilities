#!/bin/bash

SCRIPT="`readlink -e $0`"
SCRIPTPATH=$(dirname ${SCRIPT})
source ${SCRIPTPATH}/hvk-common.sh

if [ x"${DEFCONFIG_AUTOGEN}" != x"" ]; then
    if [ -x ${DEFCONFIG_AUTOGEN} ]; then
        dir=$(dirname ${DEFCONFIG_AUTOGEN})
        echo "Autogen defconfig from configuration fragments..."
        pushd ${dir}
        ./$(basename ${DEFCONFIG_AUTOGEN})
        popd
    else
        echo "Error: cannot execute configuration autogen script"
        exit 1
    fi
fi

echo "Board init: ${DEFCONFIG_BOARD}"

${KMAKE} ${DEFCONFIG_OPT}

if [ x"${1}" = x"yocto" ]; then
    LOCALVERSION="${YOCTO_LOCALVERSION}"

    # Add GIT revision to the local version
    head=`git --git-dir=.git rev-parse --verify --short HEAD 2> /dev/null`

    SCMVERSION=$(printf "%s%s" +g $head)
else
    SCMVERSION=""
fi

if [ x"${LOCALVERSION}" != x"" ]; then
    echo "Force local kernel version to: ${LOCALVERSION}"
    sed -e "s/^\(CONFIG_LOCALVERSION=\"\).*/\1${LOCALVERSION}\"/" -i .config
fi

echo ${SCMVERSION} > .scmversion

${KMAKE} olddefconfig
