#!/bin/sh

PROG_NAME=$(basename $0)

BZIP2_BIN=bzip2

if [ -x /usr/bin/pbzip2 ]; then
    BZIP2_BIN=pbzip2
fi

print_usage()
{
    echo "$(basename $0) -- Create bzip2 compressed TAR archive."
    echo "Usage: ${PROG_NAME} [OPTION]... DIR"
    echo "Options:"
    echo "  -f filename     Specify the base name of the generated archive."
    echo "                  Default is same name as the input directory."
}

while getopts "hf:" flag ;do
    case ${flag} in
	h)
	    print_usage
	    ;;
	f)
	    DEST=${OPTARG}
	    ;;
	?)
	    echo "${PROG_NAME}: Invalid option: ${OPTARG}."
	    echo "Try \`${PROG_NAME} --help' for more information."
	    exit 1
	    ;;
    esac
done
shift `expr "${OPTIND}" - 1`

# `$#' now represents the number of arguments after the options.
# `$1' is the first argument, etc.
if [ ${#} -eq 0 ]; then
    echo "${PROG_NAME}: Missing source directory."
    echo "Try \`${PROG_NAME} --help' for more information."
    exit 1
elif [ ${#} -gt 1 ]; then
    echo "${PROG_NAME}: Too many arguments."
    echo "Try \`${PROG_NAME} --help' for more information."
    exit 1
fi

DIR=${1%/}

if [ -z ${DEST} ]; then
    DEST=$(basename ${DIR})
fi

# Checking if directory exists.
if [ ! -d ${DIR} ]; then
    echo "$0: Directory ${DIR} not found."
    exit 1
fi

# Decompressing file to standard output and piping result to bzip2
tar cf - ${DIR} | ${BZIP2_BIN} -9 > ${DEST}.tar.bz2

exit $?
