#!/bin/bash

set -e

# Compile and validate all DT/schemas, usefull when testing each patch in a
# series.

hvk-compile.sh
hvk-dt.sh
