#!/bin/bash

PROG_NAME=$(basename $0)

print_usage()
{
    echo "${PROG_NAME} -- Conversion MP4 (x264)"
    echo "Usage: ${PROG_NAME} [OPTIONS...] FICHIER"
    echo
    echo "Options:"
    echo "  -a   Conversion trame audio AAC (défaut=copier)"
    echo "  -c   Copie trame vidéo (défaut=conversion)"
    echo "  -b   Si option \"a\" sélectionnée, bitrate (défaut=192k)"
    echo "  -q   Qualité (CRF). plus bas = meilleur (défaut=23)"
}

# Default values
bitrate=192k
aac=0
video_copy=0
crf=23

while getopts "ab:chq:" flag ; do
    case ${flag} in
	a)
            aac=1
	    ;;
	b)
            bitrate=${OPTARG}
	    ;;
        c)
            video_copy=1
	    ;;
	q)
            crf=${OPTARG}
	    ;;
	h)
	    print_usage
            exit 0
	    ;;
	?)
	    echo "${PROG_NAME}: Option invalide: ${OPTARG}."
	    echo "Essayez \`${PROG_NAME} -h' pour plus d'informations."
	    exit 1
	    ;;
    esac
done
shift `expr "${OPTIND}" - 1`

# `$#' now represents the number of arguments after the options.
# `$1' is the first argument, etc.
if [ $# -gt 1 ]; then
    echo "${PROG_NAME}: Too many arguments."
    echo "Essayez \`${PROG_NAME} -h' pour plus d'informations."
    exit 1
fi

if [ ${#} -ne 1 ]; then
    echo "${PROG_NAME}: Nom de fichier manquant."
    echo "Essayez \`${PROG_NAME} -h' pour plus d'informations."
    exit 1
fi

if [ x"${aac}" = x1 ]; then
    audio_opts="-c:a aac -b:a ${bitrate}"
else
    audio_opts="-c:a copy"
fi

if [ x"${video_copy}" = x1 ]; then
    video_opts="-c:v copy"
else
    video_opts="-c:v libx264  -preset veryslow -crf ${crf}"
fi

src="${1}"

# Checking if input file exist.
if [ ! -f "${src}" ]; then
    echo "$0: File ${src} not found."
    print_usage
    exit 1
fi

# Cut everything after the last dot using sed:
dest=`echo "${src}" | sed s/\.[^.]*$//`
dest="${dest}.mp4"

ffmpeg -i "${src}" ${video_opts} ${audio_opts} "${dest}"
