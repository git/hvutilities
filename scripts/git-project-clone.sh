#!/bin/bash

SCRIPT="`readlink -e $0`"
SCRIPTPATH=$(dirname ${SCRIPT})
source ${SCRIPTPATH}/hv-git-functions.sh

# Uncomment to have verbose debug output
##debug=1

print_usage()
{
    echo "$(basename $0) -- GIT clone branch for multiple subrepositories"
    echo "Usage: $(basename $0) [OPTIONS...] BRANCH_FROM BRANCH_NEW"
}

if [ "x${1}" = "x--help" ]; then
    print_usage
    exit 1
fi

if [ ${#} -ne 2 ]; then
    echo "Missing arguments"
    print_usage
    exit 1
fi

hv_git_validate_subrepos_list

branch_from=${1}
branch_new=${2}
rc=0

clone_repo()
{
    if [ ${#} -ne 1 ]; then
        echo "Missing repository name"
        exit 1
    fi

    local r=${1}

    echo -n "Repo ${r}: "

    pushd "${r}" 1> /dev/null

    b=${branch_new}
    exists=$(git show-ref refs/heads/${b})
    if [ -n "$exists" ]; then
        # Switch to existing branch
        vco -q ${b} 1> /dev/null
        echo "${b} (already created)"
    else
        # Create branch only if it doesn't already exist
        vco -q -b ${b} 1> /dev/null

        if [ ${?} -ne 0 ]; then
            echo "Error creating new branch: ${b}"
            rc=1
        else
            echo "${b}"
        fi
    fi

    popd 1> /dev/null
}

# First, try to update all subrepos to BRANCH_FROM

for r in ./ $(hv_git_get_subrepos_list); do
    # Make sure directory exists
    if [ ! -d "${r}" ]; then
        echo "Missing repos ${r}, skipping"
        continue
    fi

    hv_git_checkout ${r} ${branch_from}
    if [ ${?} -ne 0 ]; then
        echo "Error switching to ${branch_from} in repos ${r}"
        exit 1
    fi
done

# Then clone repo

for r in ./ $(hv_git_get_subrepos_list); do
    # Make sure directory exists
    if [ ! -d "${r}" ]; then
        echo "Missing repos ${r}, skipping"
        continue
    fi

    clone_repo ${r}
    rc=${?}
done

exit ${rc}
