#!/bin/bash

SCRIPT="`readlink -e $0`"
SCRIPTPATH=$(dirname ${SCRIPT})
source ${SCRIPTPATH}/hv-git-functions.sh

print_usage()
{
    echo "$(basename $0) -- Create tag from branch"
    echo "Usage: $(basename $0) [OPTIONS...] BRANCH TAG"
}

if [ "x${1}" = "x--help" ]; then
    print_usage
    exit 1
fi

if [ ${#} -ne 2 ]; then
    echo "Missing arguments"
    print_usage
    exit 1
fi

hv_git_validate_subrepos_list

branch_name=${1}
tag_name=${2}

for r in ./ $(hv_git_get_subrepos_list); do
    # Make sure directory exists
    if [ ! -d "${r}" ]; then
        echo "${SCRIPT}: missing repos ${r}, skipping"
        continue
    fi

    hv_git_tag_from_branch ${r} ${branch_name} ${tag_name}
done
