#!/usr/bin/perl

# Taken from http://rasterweb.net/raster/code/crypt.html
#
# I needed this for creating shadow passwords under Red Hat Linux 6.1, since
# passwords are not stored in plain text (that's a good idea) if I remember
# correctly, Red Hat had broken useradd, or adduser, or whatever the hell they
# were using at the time...

srand (time());
my $randletter = "(int (rand (26)) + (int (rand (1) + .5) % 2 ? 65 : 97))";
my $salt = sprintf ("%c%c", eval $randletter, eval $randletter);
my $plaintext = shift;
my $crypttext = crypt ($plaintext, $salt);

print "${crypttext}\n";
